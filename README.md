# SSCO Demo Easy Install

# Project  Summary:

This project aims to streamline the installation process of FastLane systems for demo purposes, by providing a cloud-based installation.

 

# How it works:

Using MS Flow, QA / PS can trigger the upgrade installation process of FastLane system, by specifying the target machine or FastLane's IP Address, target release version to upgrade, and calling the SSCO Demo Easy Install Web API. QA / PS is also given the option to select / deselect optional sub-components or hotfixes to install.

The Web API will download the Setup Factory deployment package, according to the selection, and push the deployment package to FastLane system. The deployment package will then prepare the system for upgrade installation process. The preparation process includes ensuring the Core Application, CADD, LaunchPad, and other key applications and key services are stopped or not running, user has an admin rights to run the installation, among others. Installation / upgrade process includes installing the selected sub-components and other required hotfixes and patches. A post-installation process will also be included to ensure the FastLane state is restored to it's use-able state after the upgrade process. All of these will run without manual intervention.  In addition, the installation / upgrade status will then be reported back to QA / PS via MS Flow.

 

# Benefits:

* Eliminates the manual and tedious upgrade installation process, from downloading of the key installers, prepping up the FastLane system for installation, until preparing and restoring the system to its use-able state.
* Provides the capability to do installation / upgrade process remotely (as long as the target machine, FastLane, is connected to a hosting server)
* Provides the option to install or skip optional sub-components or hotfixes, by providing the list of sub-components to QA / PS upon selection of target release version. 

# Scope:

* FastLane NCR Base demo upgrade installation only 
 

# Technologies Used:

* .NET Core for Web API

* Setup Factory for deployment package

* MS Flow for UI layer

